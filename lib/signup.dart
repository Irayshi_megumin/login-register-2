import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/home.dart';

class SignupPage extends StatefulWidget {
  static const routeName = '/signup';

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final TextEditingController _notelpController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _notelpController.dispose();
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Future<void> register(String email, String password, String phone) async {
    await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((value) => {postDetailsToFirestore(email, phone)})
        .catchError((e) {
      print('Error getting document: $e');
    });
  }

  postDetailsToFirestore(String email, String phone) async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    Map<String, dynamic> userData = {'email': email, 'phone': phone};

    // if (role == 'Guru') {
    //   userData['mata_kuliah'] = mata_kuliah;
    // }

    ref.doc(FirebaseAuth.instance.currentUser!.uid).set(userData);
    final docRef = ref.doc(FirebaseAuth.instance.currentUser!.uid);
    docRef.get().then(
      (DocumentSnapshot doc) {
        // final data = doc.data() as Map<String, dynamic>;
        // Navigator.of(context).pushReplacement(
        //     MaterialPageRoute(builder: (context) => HomeScreen()));
        Navigator.pushReplacementNamed(context, HomePage.routeName);
      },
      onError: (e) => print("Error getting document: $e"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40.0),
              Text(
                'EcoDeal',
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
              ),
              SizedBox(height: 0.0),
              Image.asset(
                'assets/images/logo.png',
                height: 200,
                fit: BoxFit.scaleDown,
              ),
              SizedBox(height: 60.0),
              TextFormField(
                controller: _notelpController,
                decoration: InputDecoration(
                  labelText: 'Phone Number',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(color: Color.fromARGB(255, 0, 0, 0)),
                  ),
                ),
                style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
              ),
              SizedBox(height: 16.0),
              TextFormField(
                controller: _usernameController,
                decoration: InputDecoration(
                  labelText: 'Email Address',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(color: Color.fromARGB(255, 0, 0, 0)),
                  ),
                ),
                style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
              ),
              SizedBox(height: 16.0),
              TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                  labelText: 'Password',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(color: Color.fromARGB(255, 0, 0, 0)),
                  ),
                ),
                obscureText: true,
                style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
              ),
              SizedBox(height: 16.0),
              Center(
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: 50,
                  child: ElevatedButton(
                    onPressed: () async {
                      // Perform sign up here

                      // Navigates to Home page and replaces the current page
                      // Navigator.pushReplacementNamed(
                      //     context, HomePage.routeName);

                      // =========== baru ditambahkan
                      try {
                        await register(_usernameController.text,
                            _passwordController.text, _notelpController.text);
                      } catch (e) {
                        final snackbar =
                            SnackBar(content: const Text('Register gagal'));
                        ScaffoldMessenger.of(context).showSnackBar(snackbar);
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(255, 141, 179, 255),
                      onPrimary: Color.fromARGB(255, 0, 0, 0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      minimumSize: Size(120, 50),
                    ),
                    child: Text(
                      'Sign Up',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              Align(
                alignment: Alignment.bottomCenter,
                child: TextButton(
                  onPressed: () {
                    // Navigate to login page
                    Navigator.pop(context);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account?',
                        style: TextStyle(
                          color: Color.fromARGB(255, 22, 0, 6),
                        ),
                      ),
                      Text(
                        ' Login',
                        style: TextStyle(
                          color: Color.fromARGB(255, 22, 0, 6),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
