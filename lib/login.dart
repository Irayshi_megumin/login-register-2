import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/home.dart';
import 'package:flutter_application_1/signup.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

// ... kode lainnya ...

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (FirebaseAuth.instance.currentUser != null) {
      getLogin();
    }
  }

  Future getLogin() async {
    final docRef = FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser!.uid);
    docRef.get().then(
      (DocumentSnapshot doc) {
        final data = doc.data() as Map<String, dynamic>;
        // role = data['role'];
        // if (role == 'Guru') {
        //   Navigator.of(context).pushReplacement(
        //       MaterialPageRoute(builder: (context) => HomeScreen()));
        // }
        // if (role == 'Siswa') {
        //   Navigator.of(context).pushReplacement(
        //       MaterialPageRoute(builder: (context) => HomeScreen()));
        // }
        if (data.isNotEmpty) {
          Navigator.pushReplacementNamed(context, HomePage.routeName);
        } else {
          print('Belum login');
        }
      },
      onError: (e) => print("Error getting document: $e"),
    );
  }

  Future login(String email, String password) async {
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    final docRef = FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser!.uid);
    docRef.get().then(
      (DocumentSnapshot doc) {
        final data = doc.data() as Map<String, dynamic>;
        // role = data['role'];
        // if (role == 'Guru') {
        //   Navigator.of(context).pushReplacement(
        //       MaterialPageRoute(builder: (context) => HomeScreen()));
        // }
        if (data.isNotEmpty) {
          Navigator.pushReplacementNamed(context, HomePage.routeName);
        } else {
          print('Belum login');
        }
      },
      onError: (e) => print("Error getting document: $e"),
    );
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40.0),
              Text(
                'EcoDeal',
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
              ),
              SizedBox(height: 0.0),
              Image.asset(
                'assets/images/logo.png',
                height: 200,
                fit: BoxFit.scaleDown,
              ),
              SizedBox(height: 60.0),
              TextFormField(
                controller: _usernameController,
                decoration: InputDecoration(
                  labelText: 'Email Address',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(color: Color.fromARGB(255, 0, 0, 0)),
                  ),
                ),
                style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
              ),
              SizedBox(height: 16.0),
              TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                  labelText: 'Password',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(color: Color.fromARGB(255, 0, 0, 0)),
                  ),
                ),
                obscureText: true,
                style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
              ),
              SizedBox(height: 16.0),
              Center(
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: 50,
                  child: ElevatedButton(
                    onPressed: () async {
                      // // Perform login here
                      // // Example code to simulate successful login
                      // bool loginSuccess = true;

                      // if (loginSuccess) {
                      //   Navigator.pushReplacementNamed(
                      //       context, HomePage.routeName);
                      // } else {
                      //   // Handle login failure
                      //   // Display error message or show a dialog
                      // }

                      // ================= Baru ditambahkan

                      try {
                        await login(
                            _usernameController.text, _passwordController.text);
                      } catch (e) {
                        final snackbar =
                            SnackBar(content: const Text('Login gagal'));
                        ScaffoldMessenger.of(context).showSnackBar(snackbar);
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(255, 141, 179, 255),
                      onPrimary: Color.fromARGB(255, 0, 0, 0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      minimumSize: Size(120, 50),
                    ),
                    child: Text(
                      'Login',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              Align(
                alignment: Alignment.bottomCenter,
                child: TextButton(
                  onPressed: () {
                    // Navigate to sign up page
                    Navigator.pushNamed(context, SignupPage.routeName);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Don\'t have an account?',
                        style: TextStyle(
                          color: Color.fromARGB(255, 22, 0, 6),
                        ),
                      ),
                      Text(
                        ' Sign Up',
                        style: TextStyle(
                            color: Color.fromARGB(255, 22, 0, 6),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
