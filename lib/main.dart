import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/chat_page.dart';
import 'package:flutter_application_1/firebase_options.dart';
import 'package:flutter_application_1/home.dart';
import 'package:flutter_application_1/login.dart';
import 'package:flutter_application_1/pesanan.dart';
import 'package:flutter_application_1/profil.dart';
import 'package:flutter_application_1/signup.dart';
import 'package:flutter_application_1/ubahakun.dart';
import 'package:flutter_application_1/detail_barang.dart';
import 'package:flutter_application_1/pembayaran.dart';
import 'package:flutter_application_1/pemabayaran_sukses.dart';

void main() async {
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (context) => LoginPage(),
        ProfilePage.routeName: (context) => ProfilePage(),
        HomePage.routeName: (context) => HomePage(),
        SignupPage.routeName: (context) => SignupPage(),
        PesananPage.routeName: (context) => PesananPage(),
        UbahAkunPage.routeName: (context) => UbahAkunPage(),
        ChattingPage.routeName: (context) => ChattingPage(),
        DetailBarang.routeName: (context) => DetailBarang(),
        PembayaranPage.routeName: (context) => PembayaranPage(),
        PembayaranPage.routeName: (context) => PembayaranPage(),
        PembayaranSuksesPage.routeName: (context) => PembayaranSuksesPage()
      },
    );
  }
}
