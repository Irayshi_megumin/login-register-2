import 'package:flutter/material.dart';

class PembelianPage extends StatelessWidget {
  static const routeName = '/pembelian';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pembelian'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Daftar Pembelian',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: ListView(
                children: [
                  PembelianCard(
                    productName: 'Ban Bekas',
                    price: 'Rp 50.000,00',
                    image: 'assets/images/ban.png',
                    date: '5 Juli 2023',
                  ),
                  SizedBox(height: 16),
                  PembelianCard(
                    productName: 'Botol Bekas',
                    price: 'Rp 25.000,00',
                    image: 'assets/images/botolbekas.png',
                    date: '3 Juli 2023',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PembelianCard extends StatelessWidget {
  final String productName;
  final String price;
  final String image;
  final String date;

  const PembelianCard({
    required this.productName,
    required this.price,
    required this.image,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 100,
              height: 100,
              child: Image.asset(
                image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(width: 16),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  productName,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  price,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  'Tanggal Pembelian: $date',
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
                SizedBox(height: 16),
                ElevatedButton(
                  onPressed: () {
                    // Implement your logic when "Beli Lagi" button is pressed
                    // For example, navigate to a product detail page
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue,
                  ),
                  child: Text('Beli Lagi'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
