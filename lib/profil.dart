import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/login.dart';
import 'package:flutter_application_1/ubahakun.dart';

class ProfilePage extends StatelessWidget {
  static const routeName = '/profile';

  void dialogPeringatan(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Text('Yakin ingin keluar dari akun ini?'),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            Navigator.of(context, rootNavigator: false)
                                .pop('dialog');
                          },
                          child: Text('Batal')),
                      SizedBox(
                        width: 10,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            FirebaseAuth.instance.signOut();
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()),
                            );
                          },
                          child: Text('Keluar'))
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Profil',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Container(
                color: Colors.blue,
                height: 200,
                width: double.infinity,
                child: Center(
                  child: CircleAvatar(
                    radius: 80,
                    backgroundImage: NetworkImage(
                      'https://example.com/avatar.jpg',
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 16),
              ListTile(
                leading: const Icon(Icons.person),
                title: Text('Nama'),
                subtitle: Text(
                  'Indra Santika',
                ),
              ),
              const Divider(),
              ListTile(
                leading: const Icon(Icons.account_balance_wallet),
                title: Text('Saldo'),
                subtitle: Text(
                  'Rp 10.000.000',
                ),
              ),
              const Divider(),
              ListTile(
                leading: const Icon(Icons.cake),
                title: Text('Tanggal Lahir'),
                subtitle: Text(
                  '21 Juni 2002',
                ),
              ),
              const Divider(),
              ListTile(
                leading: const Icon(Icons.phone),
                title: Text('Telepon'),
                subtitle: Text(
                  '08123456789',
                ),
              ),
              const Divider(),
              ListTile(
                leading: const Icon(Icons.email),
                title: Text('Email'),
                subtitle: Text(
                  'indra.santika@undiksha.ac.id',
                ),
              ),
              const Divider(),
              ListTile(
                leading: const Icon(Icons.wc),
                title: Text('Jenis Kelamin'),
                subtitle: Text(
                  'Laki-laki',
                ),
              ),
              const Divider(),
              ListTile(
                leading: const Icon(Icons.home),
                title: Text('Alamat'),
                subtitle: Text(
                  'Banyuning',
                ),
              ),
              const SizedBox(height: 16),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, UbahAkunPage.routeName);
                    },
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(150, 50),
                    ),
                    child: const Text('Ubah'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      // Aksi saat tombol "Logout" ditekan
                      // Navigator.pushReplacement(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => LoginPage()),
                      // );
                      dialogPeringatan(context);
                    },
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(150, 50),
                    ),
                    child: const Text('Logout'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
