import 'package:flutter/material.dart';
import 'package:flutter_application_1/pesan.dart';
import 'package:flutter_application_1/profil.dart';
import 'package:flutter_application_1/pesanan.dart';
import 'package:flutter_application_1/detail_barang.dart';

class HomePage extends StatelessWidget {
  static const routeName = '/home';

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 250,
              child: Stack(
                children: [
                  Positioned(
                    left: 0,
                    top: 0,
                    child: Container(
                      width: screenWidth,
                      height: 200,
                      color: Color(0xff7681c6),
                    ),
                  ),
                  Positioned(
                    left: 14,
                    top: 18,
                    child: Container(
                      width: screenWidth - 110,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(29),
                        color: Color(0xfff0ede5),
                      ),
                      child: Row(
                        children: [
                          SizedBox(width: 10),
                          Icon(Icons.search),
                          SizedBox(width: 10),
                          Expanded(
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: 'Search...',
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    right: 14,
                    top: 18,
                    child: Row(
                      children: [
                        IconButton(
                          icon: Icon(Icons.camera_alt),
                          tooltip: 'Camera',
                          onPressed: () {
                            // Aksi ketika ikon kamera ditekan
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.shopping_cart),
                          tooltip: 'Shopping Cart',
                          onPressed: () {
                            // Aksi ketika ikon keranjang ditekan
                          },
                        ),
                        IconButton(
                            icon: Icon(Icons.mail),
                            tooltip: 'chat',
                            onPressed: () {
                              // Aksi ketika ikon pesan ditekan
                              Navigator.pushNamed(context, ChatPage.routeName);
                            }),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 100,
                    top: 120,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: ElevatedButton(
                        onPressed: () {
                          // Aksi ketika tombol ditekan
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color.fromARGB(255, 244, 244, 244),
                        ),
                        child: Text(
                          'Posting Artikel',
                          style: TextStyle(
                            fontSize: 15,
                            color: Color.fromARGB(255, 0, 0, 0),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 100 + (screenWidth - 180) / 2,
                    top: 120,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: ElevatedButton(
                        onPressed: () {
                          // Aksi ketika tombol ditekan
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color.fromARGB(255, 255, 255, 255),
                        ),
                        child: Text(
                          'Posting Barang',
                          style: TextStyle(
                            fontSize: 15,
                            color: Color.fromARGB(255, 0, 0, 0),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Row(
              children: [
                SizedBox(width: 20),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Card(
                      child: InkWell(
                        onTap: () {
                          // Navigasi ke halaman detail barang saat gambar ditekan
                          Navigator.pushNamed(context, DetailBarang.routeName);
                        },
                        child: Container(
                          height: 200,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/ban.png',
                                fit: BoxFit.cover,
                                width: double.infinity,
                                height: 120,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(25.0),
                                child: Column(
                                  children: [
                                    Text('Ban Bekas'),
                                    Text(
                                      'Rp 50.000,00',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Card(
                      child: InkWell(
                        onTap: () {
                          // Navigasi ke halaman detail barang saat gambar ditekan
                          Navigator.pushNamed(context, DetailBarang.routeName);
                        },
                        child: Container(
                          height: 200,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/ban.png',
                                fit: BoxFit.cover,
                                width: double.infinity,
                                height: 120,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(25.0),
                                child: Column(
                                  children: [
                                    Text('Ban Bekas'),
                                    Text(
                                      'Rp 50.000,00',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Row(
              children: [
                SizedBox(width: 20),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Card(
                      child: Container(
                        height: 200,
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/images/botolbekas.png',
                              fit: BoxFit.cover,
                              width: double.infinity,
                              height: 120,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(25.0),
                              child: Column(
                                children: [
                                  Text('Botol Bekas'),
                                  Text(
                                    'Rp 25.000,00',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Card(
                      child: Container(
                        height: 200,
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/images/botolbekas.png',
                              fit: BoxFit.cover,
                              width: double.infinity,
                              height: 120,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(25.0),
                              child: Column(
                                children: [
                                  Text('Botol Bekas'),
                                  Text(
                                    'Rp 25.000,00',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            // Add more widgets below that can be scrolled
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 56,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                icon: Icon(Icons.assignment),
                tooltip: 'Pesanan',
                onPressed: () {
                  Navigator.pushNamed(context, PesananPage.routeName);
                },
              ),
              IconButton(
                icon: Icon(Icons.shopping_cart),
                tooltip: 'Shopping Cart',
                onPressed: () {
                  // Aksi ketika ikon keranjang ditekan
                },
              ),
              IconButton(
                icon: Icon(Icons.person),
                tooltip: 'Profile',
                onPressed: () {
                  Navigator.pushNamed(context, ProfilePage.routeName);
                  // Aksi ketika ikon profile ditekan
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
